module.exports = function () {
    function getArduinos() {
        return [
            { port: '143.38.93.36', serialNumber: '297375164', apiOK: true, imuId: 1 },
            { port: '3286.372.2972.2', serialNumber: '927735652', apiOK: true, imuId: 2 },
            { port: '872.1083.287', serialNumber: 'I71863681', apiOK: false, imuId: 3 },
            { port: '19.275.28', serialNumber: '9273RT1', imuId: 4 },
        ];
    }

    return {
        getArduinos,
    };
};
