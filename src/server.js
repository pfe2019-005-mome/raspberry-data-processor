const server = require('http').createServer();
const io = require('socket.io')(server);

let nbrClientConnected = -1;
let nbrShoulderData = 0;
let nbrElbowData = 0;
let nbrWristData = 0;
let nbrSwordData = 0;

io.on('connection', function (socket) {

    nbrClientConnected++;

    io.emit('nbr-client-connected', nbrClientConnected);
    console.log('client connected : ', socket.id);

    socket.on('on-sensor-shoulder', function (data) {
        nbrShoulderData++;
        socket.broadcast.emit('shoulder-imu', data);
    });
    socket.on('on-sensor-elbow', function (data) {
        nbrElbowData++;
        socket.broadcast.emit('elbow-imu', data);
    });
    socket.on('on-sensor-wrist', function (data) {
        nbrWristData++;
        socket.broadcast.emit('wrist-imu', data);
    });
    socket.on('on-sensor-sword', function (data) {
        nbrSwordData++;
        socket.broadcast.emit('sword-imu', data);
    });

    socket.on('disconnect', function () {
        nbrClientConnected--;
        io.emit('nbr-client-connected', nbrClientConnected);
        console.log('client disconnect...', socket.id);
    });

    socket.on('error', function (err) {
        console.log('received error from client:', socket.id);
        console.log(err)
    })
});

server.listen(3000, function (err) {
    if (err) throw err;
    console.log('listening on port 3000')
});
