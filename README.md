# Server-data-processor

Get data from the raspberry and give it to all clients.
This project works with [socket.io](https://socket.io/) version 2.0.

## Environment

Before you begin, make sure your development environment includes Node.js version 10.9.0 or later and an npm package manager.
Run `npm install` at the project's root to install dependencies.

## Development server

Run `npm start` for a dev server. The server will be launch on `http://localhost:3000/`.

## Build

Run `npm run build` to build the project.

## Running unit tests

Run `npm run test` to execute the unit tests.